// This sample shows adding an action to an [AppBar] that opens a shopping cart.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _MyApp(),
    );
  }
}

class _MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Lists'),
      ),
      body:
          MyProductLists(),
    );
  }
}

class CustomWidgetListTile extends StatelessWidget {
  CustomWidgetListTile({this.productName, this.productPrice});
  final String productName;
  final String productPrice;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.0),
      height: 75.0,
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Stack(
                  children: <Widget>[
                    Container(
                      color: Colors.amberAccent,
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.all(10.0),
                        color: Colors.blue,
                      ),
                    )
                  ],
                )),
            Expanded(
              flex: 4,
              child: Container(
                color: Colors.red,
              ),
            ),
            Container(
              width: 10.0,
              color: Colors.blue,
            )
          ],
        ),
      ),
    );
  }
}

class MyProductLists extends StatelessWidget {
  final List<String> productsName = ['Burger', 'Hamburger', 'Cheese Burger'];
  final List<String> productsPrice = ['10000', '20000', '30000'];
  final List<int> colorCodes = <int>[600, 500, 100];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: productsName.length,
      separatorBuilder: (BuildContext context, int index) => Divider(),
      itemBuilder: (BuildContext context, int index) {
        return CustomWidgetListTile(
            productName: productsName[index],
            productPrice: productsPrice[index]);
      },
    );
  }
}
