import 'dart:math';

class Rectangle {
  int width;
  int height;
  Point origin;

  // Simple Constructor easier than create overloading methods
  Rectangle({this.origin = const Point(0, 0), this.width = 0, this.height = 0});

  @override
  String toString() => 'Origin: $origin, width: $width, height: $height';

}

void main() {
  print(Rectangle(origin: const Point(10, 20), width: 100, height: 200));
  print(Rectangle(origin: const Point(10, 20), width: 100));
  print(Rectangle(width: 200));
  print(Rectangle());
}