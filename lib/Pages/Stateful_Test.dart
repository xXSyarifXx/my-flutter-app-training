// This sample shows adding an action to an [AppBar] that opens a shopping cart.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyStateful(
        startingNumber: 0,
      ),
    );
  }
}

class MyStateful extends StatefulWidget {
  final int startingNumber;
  MyStateful({this.startingNumber});

  @override
  State<StatefulWidget> createState() {
    return _MyStateful();
  }
}

class _MyStateful extends State<MyStateful> {
  int _counts;

  @override
  void initState() {
    if (widget.startingNumber != null) {
      _counts = widget.startingNumber;
    } else {
      _counts = 0;
    }
    super.initState();
  }

  void incrementTap() {
    setState(() {
      _counts += 1;
    });
  }
  void decrementTap() {
    setState(() {
      _counts -= 1;
    });
  }

  @override
  Widget build(BuildContext context) {
  //test
    return Scaffold(
      appBar: AppBar(
        title: Text('Stateful Test'),
      ),
      body: Center(
        child: Text('Button Pressed: $_counts'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FloatingActionButton(
                onPressed: incrementTap,
                child: Icon(Icons.add),
              ),
              FloatingActionButton(
                onPressed: decrementTap,
                child: Icon(Icons.remove),
              ),
            ],
          )),
    );
  }
}
